package com.emrekose.githubmvp.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.emrekose.githubmvp.GithubApp;
import com.emrekose.githubmvp.R;
import com.emrekose.githubmvp.di.detail.DaggerRepoDetailComponent;
import com.emrekose.githubmvp.di.detail.RepoDetailModule;
import com.emrekose.githubmvp.event.RepoDetailEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RepoDetailActivity extends AppCompatActivity {

    @Inject
    EventBus eventBus;

    @BindView(R.id.detailToolbar)
    Toolbar detailToolbar;

    @BindView(R.id.detailRepoName)
    AppCompatTextView detailRepoName;

    @BindView(R.id.detailDescription)
    AppCompatTextView detailDescription;

    @BindView(R.id.detailLanguage)
    AppCompatTextView detailLanguage;

    @BindView(R.id.detailUrl)
    AppCompatTextView detailUrl;

    @BindView(R.id.userAvatar)
    ImageView userAvatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_detail);
        ButterKnife.bind(this);

        setSupportActionBar(detailToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);

        initInjector();

    }

    @Subscribe(sticky = true)
    public void onRepoDetailEvent(RepoDetailEvent event) {
        detailRepoName.setText(event.getRepository().getName());
        detailDescription.setText(event.getRepository().getDescription());
        detailLanguage.setText(event.getRepository().getLanguage());
        detailUrl.setText(event.getRepository().getUrl());

        Glide.with(this)
                .load(event.getRepository().getOwner().getAvatar_url())
                .into(userAvatar);
    }

    @Override
    public void onStart() {
        super.onStart();
        eventBus.register(this);
    }

    @Override
    public void onStop() {
        eventBus.unregister(this);
        super.onStop();
    }

    private void initInjector() {
        DaggerRepoDetailComponent.builder()
                .appComponent(GithubApp.get(this).getAppComponent())
                .repoDetailModule(new RepoDetailModule())
                .build()
                .inject(this);
    }
}
