package com.emrekose.githubmvp.mvp.presenter.list;

/**
 * Created by emrekose on 25.02.2017.
 */

public interface IRepoPresenter {
    void loadUserRepos(String username);
}
