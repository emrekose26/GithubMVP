package com.emrekose.githubmvp.di.detail;

import com.emrekose.githubmvp.di.PerActivity;
import com.emrekose.githubmvp.ui.activity.RepoDetailActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by emrekose on 26.02.2017.
 */

@Module
public class RepoDetailModule {

    RepoDetailActivity activity;

    // // TODO: 26.02.2017 view al constructor ver

    @Provides
    @PerActivity
    RepoDetailActivity provideDetailActivity() {
        return activity;
    }

    // todo presenter provide et
}
