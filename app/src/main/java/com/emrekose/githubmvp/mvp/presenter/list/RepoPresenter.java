package com.emrekose.githubmvp.mvp.presenter.list;

import com.emrekose.githubmvp.model.api.ApiService;
import com.emrekose.githubmvp.mvp.view.list.IRepoView;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by emrekose on 25.02.2017.
 */

public class RepoPresenter implements IRepoPresenter {

    private IRepoView repoView;
    private ApiService apiService;

    @Inject
    public RepoPresenter(IRepoView repoView, ApiService apiService) {
        this.repoView = repoView;
        this.apiService = apiService;
    }

    @Override
    public void loadUserRepos(String username) {
        repoView.showProgress();

        apiService.getUserRepos(username)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(repositoryList -> {
                            repoView.hideProgress();
                            repoView.showUserRepos(repositoryList);
                        },
                        e -> Timber.e(e.getMessage()));
    }
}
