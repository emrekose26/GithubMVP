package com.emrekose.githubmvp;

import android.app.Application;
import android.content.Context;

import com.emrekose.githubmvp.di.app.AppComponent;
import com.emrekose.githubmvp.di.app.AppModule;
import com.emrekose.githubmvp.di.app.DaggerAppComponent;
import com.emrekose.githubmvp.di.app.NetworkModule;

import timber.log.Timber;

/**
 * Created by emrekose on 25.02.2017.
 */

public class GithubApp extends Application {

    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initInjector();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    private void initInjector() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();
    }

    public static GithubApp get(Context context) {
        return (GithubApp) context.getApplicationContext();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
