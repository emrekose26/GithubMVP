package com.emrekose.githubmvp.di.list;

import com.emrekose.githubmvp.di.PerActivity;
import com.emrekose.githubmvp.di.app.AppComponent;
import com.emrekose.githubmvp.ui.activity.RepoActivity;

import dagger.Component;

/**
 * Created by emrekose on 25.02.2017.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules = RepoModule.class)
public interface RepoComponent {
    void inject(RepoActivity activity);
}
