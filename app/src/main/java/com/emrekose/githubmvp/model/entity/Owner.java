package com.emrekose.githubmvp.model.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by emrekose on 25.02.2017.
 */

public class Owner {
    @SerializedName("login")
    private String login;

    @SerializedName("avatar_url")
    private String avatar_url;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }
}
