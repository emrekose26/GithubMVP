package com.emrekose.githubmvp.model.api;

import com.emrekose.githubmvp.model.entity.Repository;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by emrekose on 25.02.2017.
 */

public interface ApiService {

    @GET("users/{username}/repos")
    Observable<List<Repository>> getUserRepos(@Path("username") String username);
}
