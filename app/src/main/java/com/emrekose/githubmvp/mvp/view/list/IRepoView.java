package com.emrekose.githubmvp.mvp.view.list;

import com.emrekose.githubmvp.model.entity.Repository;

import java.util.List;

/**
 * Created by emrekose on 25.02.2017.
 */

public interface IRepoView {
    void showProgress();

    void hideProgress();

    void showUserRepos(List<Repository> repositoryList);
}
