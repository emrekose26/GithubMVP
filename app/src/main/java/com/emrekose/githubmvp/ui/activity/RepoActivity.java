package com.emrekose.githubmvp.ui.activity;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.emrekose.githubmvp.GithubApp;
import com.emrekose.githubmvp.R;
import com.emrekose.githubmvp.di.list.DaggerRepoComponent;
import com.emrekose.githubmvp.di.list.RepoModule;
import com.emrekose.githubmvp.event.RepoDetailEvent;
import com.emrekose.githubmvp.model.entity.Repository;
import com.emrekose.githubmvp.mvp.presenter.list.RepoPresenter;
import com.emrekose.githubmvp.mvp.view.list.IRepoView;
import com.emrekose.githubmvp.ui.adapter.RepoRecyclerViewAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RepoActivity extends AppCompatActivity implements IRepoView {

    @BindView(R.id.mainToolbar)
    Toolbar toolbar;

    @BindView(R.id.repoListRecyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Inject
    RepoPresenter presenter;

    @Inject
    EventBus eventBus;

    RepoRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo);
        ButterKnife.bind(this);

        initInjector();

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);

    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showUserRepos(List<Repository> repositoryList) {
        adapter = new RepoRecyclerViewAdapter(repositoryList,this,eventBus);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                presenter.loadUserRepos(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onStart() {
        super.onStart();
        eventBus.register(this);
    }

    @Override
    protected void onStop() {
        eventBus.unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onRepoDetailEvent(RepoDetailEvent event){}

    private void initInjector() {
        DaggerRepoComponent.builder()
                .appComponent(GithubApp.get(this).getAppComponent())
                .repoModule(new RepoModule(this))
                .build()
                .inject(this);
    }
}
