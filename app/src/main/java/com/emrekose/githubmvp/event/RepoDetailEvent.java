package com.emrekose.githubmvp.event;

import com.emrekose.githubmvp.model.entity.Repository;

/**
 * Created by emrekose on 26.02.2017.
 */

public class RepoDetailEvent {

    public Repository repository;

    public RepoDetailEvent(Repository repository) {
        this.repository = repository;
    }

    public Repository getRepository() {
        return repository;
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }
}
