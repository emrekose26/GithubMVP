package com.emrekose.githubmvp.di.detail;

import com.emrekose.githubmvp.di.PerActivity;
import com.emrekose.githubmvp.di.app.AppComponent;
import com.emrekose.githubmvp.ui.activity.RepoDetailActivity;

import dagger.Component;

/**
 * Created by emrekose on 26.02.2017.
 */

@PerActivity
@Component(modules = RepoDetailModule.class, dependencies = AppComponent.class)
public interface RepoDetailComponent {
    void inject(RepoDetailActivity activity);
}
