package com.emrekose.githubmvp.di.list;

import com.emrekose.githubmvp.di.PerActivity;
import com.emrekose.githubmvp.model.api.ApiService;
import com.emrekose.githubmvp.mvp.presenter.list.RepoPresenter;
import com.emrekose.githubmvp.mvp.view.list.IRepoView;
import com.emrekose.githubmvp.ui.activity.RepoActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by emrekose on 25.02.2017.
 */

@Module
public class RepoModule {

    IRepoView repoView;
    RepoActivity activity;

    public RepoModule(IRepoView repoView) {
        this.repoView = repoView;
    }

    @Provides
    @PerActivity
    RepoActivity provideActivity() {
        return activity;
    }

    @Provides
    @PerActivity
    RepoPresenter provideRepoPresenter(ApiService apiService) {
        return new RepoPresenter(repoView,apiService);
    }
}
