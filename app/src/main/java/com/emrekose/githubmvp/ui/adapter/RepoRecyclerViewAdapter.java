package com.emrekose.githubmvp.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emrekose.githubmvp.R;
import com.emrekose.githubmvp.event.RepoDetailEvent;
import com.emrekose.githubmvp.model.entity.Repository;
import com.emrekose.githubmvp.ui.activity.RepoDetailActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emrekose on 25.02.2017.
 */

public class RepoRecyclerViewAdapter extends RecyclerView.Adapter<RepoRecyclerViewAdapter.ViewHolder> {

    private List<Repository> repositoryList;
    private Context context;
    private EventBus eventBus;

    @Inject
    public RepoRecyclerViewAdapter(List<Repository> repositoryList, Context context, EventBus eventBus) {
        this.repositoryList = repositoryList;
        this.context = context;
        this.eventBus = eventBus;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.starGazerCount)
        TextView starGazerCount;

        @BindView(R.id.issueCount)
        TextView issueCount;

        @BindView(R.id.repoNameTextView)
        TextView repoName;

        @BindView(R.id.languageTextView)
        TextView languageTextView;

        @BindView(R.id.repoDescription)
        TextView repoDesc;

        @BindView(R.id.layout_content)
        LinearLayout layout_content;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repo_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Repository repository = repositoryList.get(position);

        holder.repoName.setText(repository.getName());
        holder.starGazerCount.setText(String.valueOf(repository.getStargazers_count()));
        holder.issueCount.setText(String.valueOf(repository.getOpen_issues_count()));
        holder.languageTextView.setText(repository.getLanguage());
        holder.repoDesc.setText(repository.getDescription());

        holder.layout_content.setOnClickListener(view -> {
            eventBus.postSticky(new RepoDetailEvent(repository));
            context.startActivity(new Intent(context, RepoDetailActivity.class));
        });
    }

    @Override
    public int getItemCount() {
        return repositoryList.size();
    }


}
