package com.emrekose.githubmvp.di.app;

import com.emrekose.githubmvp.model.api.ApiService;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by emrekose on 25.02.2017.
 */

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface AppComponent {
    ApiService apiService();
    EventBus bus();
}
